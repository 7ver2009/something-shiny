import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule,  } from '@angular/common/http';

//Layout Modules
import { CommonLayoutComponent } from './common/common-layout.component';
import { AuthenticationLayoutComponent } from './common/authentication-layout.component';

//Directives
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Sidebar_Directives } from './shared/directives/side-nav.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

// Routing Module
import { AppRoutes } from './app.routing';

// App Component
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { AuthGuardService } from './service/auth-guard.service';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(AppRoutes, 
            { useHash: true }),
        NgbModule.forRoot(),
        FormsModule,
        PerfectScrollbarModule,        
        HttpClientModule            
    ],
    declarations: [
        AppComponent,
        CommonLayoutComponent,
        AuthenticationLayoutComponent,
        Sidebar_Directives,
        AuthComponent        
    ],
    providers: [AuthGuardService],
    bootstrap: [AppComponent]
})


export class AppModule { }

import { AuthComponent } from './auth/auth.component';
import { Routes} from '@angular/router';
import { CommonLayoutComponent } from './common/common-layout.component';
import { AuthGuardService } from './service/auth-guard.service';

export const AppRoutes: Routes = [
    {
        path: '',
        component: AuthComponent
    },
    {
        path: 'dashboard',
        component: CommonLayoutComponent,
        canActivate: [AuthGuardService],        
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'                
            }            
        ]
    },
    {
        path: '**',
        redirectTo: ''
        // pathMatch: 'full',
    }
];


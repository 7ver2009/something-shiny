import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

@Injectable()
export class HttpAuthService {
  private _url = "http://devops.hezzl.com/admin/login";  
  
  constructor(private http:HttpClient) { }
  
  sendAuthData(data:FormData):Observable<Object>
  {
    return this.http.post(this._url, data)
  }
}

import { Injectable } from '@angular/core';
import { CanActivate } from '../../../node_modules/@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate { 
  
  constructor() {   
  }
  // Jwt-Helper ваш токен не читает поэтому сделал так примитивно
  canActivate = function():boolean
  {
    if(sessionStorage.getItem("token")){
      return true
    }
    return false
  }
}
import { Component, OnInit } from '@angular/core';
import { HttpAuthService } from '../service/http-auth.service';
import { Router } from '../../../node_modules/@angular/router';
import { NgForm } from '../../../node_modules/@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers: [HttpAuthService]  
})
export class AuthComponent implements OnInit {

  private incorrectInput = false;
  private connectError = false;
  
  constructor(
    private httpGet:HttpAuthService,
    private router:Router) {  };

  getData(form:NgForm):void
  {
    let loginData = new FormData();    
    loginData.append("email", form.value.email);
    loginData.append("password", form.value.password);
    
    this.httpGet.sendAuthData(loginData)
      .subscribe((data)=>{        
        if(data["token"]){          
          sessionStorage.setItem("token", data["token"]);    
          this.router.navigateByUrl("/dashboard");
        }
      },

      (error)=>{
        let errorStatus = error.status;
        if(errorStatus === 403){
          this.incorrectInput = true
        } else{this.connectError = true}
      });      
  }; 

  private resetError(){
    this.incorrectInput = false;
    this.connectError = false;
  
  }

  ngOnInit() { 
  }
}
